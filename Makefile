## ——————— MyErp ————————————————————————————————————————————————————————————————————————
DOCKER_COMPOSE         = docker compose -f "Docker/docker-compose.yml" -p my_erp
PHP_EXEC               = $(DOCKER_COMPOSE) exec php
WORKER_EXEC            = $(DOCKER_COMPOSE) exec -T php-worker

.DEFAULT_GOAL          = help
.PHONY: install clear-cache security prepare-test test test-coverage init cs fix-cs stan
help: ## Display help
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## ——————— Docker ——————————————————————————————————————————————————————————————————————————
up: ## Start the container
	DOCKER_BUILDKIT=1 $(DOCKER_COMPOSE) up -d --remove-orphans

build: ## Force build the container
	DOCKER_BUILDKIT=1 $(DOCKER_COMPOSE) up -d --build --force-recreate

stop: ## Stop the container
	@$(DOCKER_COMPOSE) stop

down: ## Remove containers
	@$(DOCKER_COMPOSE) down

sh: ## Log to container
	@$(PHP_EXEC) /bin/bash

logs: ## Show logs
	@$(DOCKER_COMPOSE) logs

## ——————— Composer ————————————————————————————————————————————————————————————————————————
install: ## Install vendors
	cp .env.dev .env
	@$(PHP_EXEC) composer install --no-progress --no-interaction --optimize-autoloader

## ——————— Symfony —————————————————————————————————————————————————————————————————————————
cc: ## Clear the cache
	@$(PHP_EXEC) php bin/console c:c

security: ## Checks if this app has known security vulnerabilities
	@$(PHP_EXEC) local-php-security-checker --path=./composer.lock

permissions: ## Set permissions
	@$(PHP_EXEC) setfacl -Rm u:app:rwX,u:www-data:rwX var
	@$(PHP_EXEC) setfacl -dRm u:app:rwX,u:www-data:rwX var

## ——————— Project —————————————————————————————————————————————————————————————————————————
init: down up install cc ## Initialize project
	@$(PHP_EXEC) php bin/console lexik:jwt:generate-keypair --skip-if-exists -n
	@$(PHP_EXEC) php bin/console doctrine:database:drop --force --if-exists
	@$(PHP_EXEC) php bin/console doctrine:database:create --if-not-exists
	@$(PHP_EXEC) php bin/console doctrine:migration:migrate -n --allow-no-migration
	@$(PHP_EXEC) php bin/console doctrine:fixtures:load -n

update: down up install cc ## Update project schema
	@$(PHP_EXEC) php bin/console doctrine:migration:diff -n
	@$(PHP_EXEC) php bin/console doctrine:migration:migrate -n --allow-no-migration

fixture: ## add fixtures to project
	@$(PHP_EXEC) php bin/console doctrine:fixtures:load -n

start: up ## Start project

reload: stop build ## Rebuild project

## ——————— Production ——————————————————————————————————————————————————————————————————————
prod-init: prod-install prod-cc ## Initialize project in production
	php bin/console lexik:jwt:generate-keypair --skip-if-exists -n --env=prod
	php bin/console doctrine:database:drop --force --if-exists --env=prod
	php bin/console doctrine:database:create --if-not-exists --env=prod 
	php bin/console doctrine:migration:migrate -n --allow-no-migration --env=prod
	php bin/console doctrine:fixtures:load -n --env=prod

## TODO: prod-update

prod-cc: ## Clear the production cache
	php bin/console c:c

prod-install: ## Install production vendors
	composer install --no-interaction --optimize-autoloader

## ——————— Tests ———————————————————————————————————————————————————————————————————————————
prepare-test: ## Prepare env test
	@$(PHP_EXEC) php bin/console c:c --env=test
	@$(PHP_EXEC) php bin/console doctrine:database:drop --force --if-exists --env=test
	@$(PHP_EXEC) php bin/console doctrine:database:create --if-not-exists --env=test
	@$(PHP_EXEC) php bin/console doctrine:migration:migrate -n --allow-no-migration --env=test
	@$(PHP_EXEC) php bin/console lexik:jwt:generate-keypair --skip-if-exists -n --env=test
	@$(PHP_EXEC) php bin/console doctrine:fixtures:load -n --env=test

test: prepare-test ## Run all tests
	@$(PHP_EXEC) vendor/phpunit/phpunit/phpunit tests/ --stop-on-error --stop-on-failure

test-coverage: prepare-test ## Run all test for coverage
	@$(PHP_EXEC) vendor/phpunit/phpunit/phpunit -c . --coverage-html coverage

## ——————— Coding Standards ————————————————————————————————————————————————————————————————
cs: ## Run all coding standards checks
	@$(PHP_EXEC) vendor/bin/php-cs-fixer fix --dry-run --diff

fix-cs: ## Run all coding standards checks
	@$(PHP_EXEC) vendor/bin/php-cs-fixer fix

stan: ## Run PHPStan
	@$(PHP_EXEC) vendor/bin/phpstan analyse --memory-limit 512M

## ——————— CI ——————————————————————————————————————————————————————————————————————————————
ci-security: ## Run security check
	./local-php-security-checker --path=./composer.lock

ci-cs: ## Run all coding standards checks
	./vendor/bin/php-cs-fixer fix --dry-run --diff

ci-stan: ## Run PHPStan
	./vendor/bin/phpstan analyse --memory-limit 512M

ci-test: ## Run all tests
	php bin/console cache:clear --env=test \
	&& php bin/console doctrine:database:drop --force --if-exists --env=test \
	&& php bin/console doctrine:database:create --if-not-exists --env=test \
	&& php bin/console doctrine:migration:migrate -n --allow-no-migration --env=test \
	&& php bin/console lexik:jwt:generate-keypair --skip-if-exists -n --env=test \
	&& php bin/console doctrine:fixtures:load -n --env=test \
	&& ./vendor/phpunit/phpunit/phpunit tests/ --stop-on-error --stop-on-failure --colors=always
