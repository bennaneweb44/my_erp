# My ERP

## Stack technique
#### Backend
* Symfony 6
* Api Platform 3
#### Frontend
* React 18
#### Conteneurisation
* Docker
* Docker-compose
#### Qualité
* PhpCs
* PhpStan
#### Tests
* PhpUnit
* Cypress
#### Inégration
* Gitlab-ci
#### Documentation
* Schéma CMD gloabal
* README technique
* PDF fonctionnel

## Installation locale « dev »
### Environnement
- Copier le fichier ".env.dev" vers ".env"

### Installation
- Lancer la commande :
```ssh
make init
```
### Commandes utiles
- Supprimer toutes les images docker d'un coup
```ssh
docker rmi -f $(docker images -aq)
```

## Installation distante « prod »
### Environnement
- Copier le fichier ".env.dev" vers ".env"
- Mettre à jour les données de connexion à la BD de l'environnement cible

### Installation
- Lancer la commande :
```ssh
make prod-init
```
