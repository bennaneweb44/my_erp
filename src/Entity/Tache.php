<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Traits\Timestamps\EntityTimestampableTrait;
use App\Repository\TacheRepository;
use App\State\Processors\TacheProcessor;
use App\State\Providers\TacheProvider;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Post(),
        new Patch(),
        new Delete(),
    ],
    normalizationContext: [
        'groups' => [
            'read:tache',
        ],
        'skip_null_values' => false,
    ],
    processor: TacheProcessor::class,
    provider: TacheProvider::class,
)]
#[ORM\Table(name: 'taches')]
#[ORM\Entity(repositoryClass: TacheRepository::class)]
#[SoftDeleteable(hardDelete: false)]
class Tache
{
    use EntityTimestampableTrait;
    use SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:tache'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:tache'])]
    private ?string $libelle = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['read:tache'])]
    private ?string $contenu = null;

    #[ORM\Column(length: 30)]
    #[Groups(['read:tache'])]
    private ?string $type = null;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => null])]
    #[Groups(['read:tache'])]
    private ?bool $faite = null;

    #[ORM\ManyToOne(inversedBy: 'taches')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:tache'])]
    private ?User $createdBy = null;

    #[ORM\ManyToOne(inversedBy: 'taches')]
    #[Groups(['read:tache'])]
    private ?Groupe $groupe = null;

    #[ORM\ManyToOne]
    private ?TachePriority $priority = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): static
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): static
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function isFaite(): ?bool
    {
        return $this->faite;
    }

    public function setFaite(?bool $faite): static
    {
        $this->faite = $faite;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): static
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): static
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getPriority(): ?TachePriority
    {
        return $this->priority;
    }

    public function setPriority(?TachePriority $priority): static
    {
        $this->priority = $priority;

        return $this;
    }
}
