<?php

namespace App\Entity\Pivot;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use App\Entity\Parametre;
use App\Entity\Traits\Timestamps\EntityTimestampableTrait;
use App\Entity\User;
use App\Repository\Pivot\UserParametreRepository;
use App\State\Processors\UserParametreProcessor;
use App\State\Providers\UserParametreProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    normalizationContext: [
        'groups' => [
            'read:userParametre',
            'read:user',
            'read:parametre',
        ],
    ],
)]
#[ORM\Table(name: 'users_parametres')]
#[ORM\Entity(repositoryClass: UserParametreRepository::class)]
#[Get(provider: UserParametreProvider::class)]
#[GetCollection(provider: UserParametreProvider::class)]
#[Patch(processor: UserParametreProcessor::class)]
#[Delete(processor: UserParametreProcessor::class)]
class UserParametre
{
    use EntityTimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:userParametre'])]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['read:userParametre', 'read:user'])]
    private ?User $user = null;

    #[ORM\ManyToOne(targetEntity: Parametre::class)]
    #[ORM\JoinColumn(name: 'parametre_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['read:userParametre', 'read:parametre'])]
    private ?Parametre $parametre = null;

    #[ORM\Column(length: 255, nullable: false)]
    #[Groups(['read:userParametre'])]
    private string $value = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getParametre(): ?Parametre
    {
        return $this->parametre;
    }

    public function setParametre(?Parametre $parametre): self
    {
        $this->parametre = $parametre;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
