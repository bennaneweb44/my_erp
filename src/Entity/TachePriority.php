<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Entity\Traits\Timestamps\EntityTimestampableTrait;
use App\Repository\TachePriorityRepository;
use App\State\Processors\TachePriorityProcessor;
use App\Tools\Front\CustomMessage;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
    ],
    normalizationContext: [
        'groups' => [
            'read:tachePriority',
        ],
    ],
)]
#[ORM\Table(name: 'tache_priorities')]
#[ORM\Entity(repositoryClass: TachePriorityRepository::class)]
#[Post(processor: TachePriorityProcessor::class)]
#[Patch(processor: TachePriorityProcessor::class)]
#[Delete(processor: TachePriorityProcessor::class)]
#[UniqueEntity('niveau', CustomMessage::UNIQUE_TACHE_PRIORITY_NIVEAU)]
class TachePriority
{
    use EntityTimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:tachePriority'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:tachePriority'])]
    private ?string $libelle = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:tachePriority'])]
    private ?string $couleur = null;

    #[ORM\Column]
    #[Groups(['read:tachePriority'])]
    private ?int $niveau = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): static
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): static
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getNiveau(): ?int
    {
        return $this->niveau;
    }

    public function setNiveau(int $niveau): static
    {
        $this->niveau = $niveau;

        return $this;
    }
}
