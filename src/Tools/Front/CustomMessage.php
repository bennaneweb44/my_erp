<?php

declare(strict_types=1);

namespace App\Tools\Front;

class CustomMessage
{
    // Responses
    final public const BAD_REQUEST = 'Demande invalide';
    final public const RESOURCE_NOT_FOUND = 'Ressource introuvable';
    final public const ACCESS_FORBIDDEN = 'Accès refusé. Vous n\'avez pas accès à cette ressource.';
    final public const ACTION_FORBIDDEN = 'Action interdite. Vous ne pouvez pas modifier cette ressource.';
    final public const UNIQUE_TACHE_PRIORITY_NIVEAU = 'Ce niveau existe déjà !';
}
