<?php

declare(strict_types=1);

namespace App\Tools\Enum;

class FixturesEnum
{
    /**************************************************************************************
     *************************************** USERS ****************************************
     **************************************************************************************/
    final public const USERS_DATA = [
        'root' => [
            'nom' => 'BENNANE',
            'prenom' => 'Aziz',
            'email' => 'asishere44@gmail.com',
            'password' => 'password',
        ],
        'admin' => [
            'nom' => 'ADMIN',
            'prenom' => 'Account',
            'email' => 'admin.account@email.com',
            'password' => 'password',
        ],
        'user' => [
            'nom' => 'User',
            'prenom' => 'Account',
            'email' => 'user.account@email.com',
            'password' => 'password',
        ],
    ];

    /**************************************************************************************
     ************************************** GROUPES ***************************************
     **************************************************************************************/
    final public const GROUPES_LIBELLES = [
        'Utilisateurs' => 'Utilisateurs',
        'Vendeurs' => 'Vendeurs',
        'Commerciaux' => 'Commerciaux',
        'Partenaires' => 'Partenaires',
        'Gestionnaire' => 'Gestionnaire',
        'Administrateurs' => 'Administrateurs',
        'Super-Administrateurs' => 'Super-Administrateurs',
    ];
}
