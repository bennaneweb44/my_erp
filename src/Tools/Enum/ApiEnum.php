<?php

declare(strict_types=1);

namespace App\Tools\Enum;

class ApiEnum
{
    // Api
    final public const DEFAULT_PAGINATION = 30;
    final public const URI_PREFIX = 'api';
    final public const API_USERS = 'users';
    final public const API_GROUPES = 'groupes';
    final public const API_AUTORISATIONS = 'autorisations';
    final public const API_TACHES = 'taches';
    final public const API_USER_PARAMETRES = 'user_parametres';
    final public const API_TACHE_PRIORITIES = 'tache_priorities';
}
