<?php

declare(strict_types=1);

namespace App\Tools\Enum;

class StaticDataEnum
{
    final public const ALL_ENTITIES = [
        'autorisations', 'groupes', 'parametres', 'taches', 'users',
    ];

    /**************************************************************************************
     ************************************** USERS *****************************************
     **************************************************************************************/
    final public const USERS_ROLES = [
        'ROLE_ROOT' => 'ROLE_ROOT',
        'ROLE_ADMIN' => 'ROLE_ADMIN',
        'ROLE_USER' => 'ROLE_USER',
    ];

    /**************************************************************************************
     ************************************** TACHES ****************************************
     **************************************************************************************/
    final public const TACHES_TYPES = [
        'Utilisateur' => 'Utilisateur',
        'Groupe' => 'Groupe',
        'Tout' => 'Tout',
    ];

    /**************************************************************************************
     *********************************** AUTORISATIONS ************************************
     **************************************************************************************/
    final public const AUTORISATIONS_OBJECTS = [
        'entity_get_all' => [
            'methode' => 'get',
            'route' => '/entity',
        ],
        'entity_get_item' => [
            'methode' => 'get',
            'route' => '/entity/{id}',
        ],
        'entity_post' => [
            'methode' => 'post',
            'route' => '/entity',
        ],
        'entity_patch' => [
            'methode' => 'patch',
            'route' => '/entity/{id}',
        ],
        'entity_delete' => [
            'methode' => 'delete',
            'route' => '/entity/{id}',
        ],
    ];

    /**************************************************************************************
     ************************************ PARAMETRES **************************************
     **************************************************************************************/
    final public const PARAMETRES_MENU_REDUIT = 'Menu réduit';
    final public const PARAMETRES_ELEMENTS_PAR_PAGE = 'Eléments par page';
    final public const PARAMETRES_OBJECTS = [
        [
            'libelle' => self::PARAMETRES_MENU_REDUIT,
            'type' => 'bool',
            'description' => 'Si coché, le menu de navigation sera réduit et les sous-liens s\'afficheront au survol.',
        ],
        [
            'libelle' => self::PARAMETRES_ELEMENTS_PAR_PAGE,
            'type' => 'int',
            'description' => 'Définit le nombre maximum d\'éléments à afficher par page, et ceci pour toutes les pages de liste.',
        ],
    ];

    /**************************************************************************************
     ********************************* TACHES PRIORITIES **********************************
     **************************************************************************************/
    final public const TACHES_PRIORITIES_OBJECTS = [
        'basse' => [
            'couleur' => 'bg-success',
            'niveau' => 3,
        ],
        'moyenne' => [
            'couleur' => 'bg-warning',
            'niveau' => 2,
        ],
        'haute' => [
            'couleur' => 'bg-danger',
            'niveau' => 1,
        ],
    ];
}
