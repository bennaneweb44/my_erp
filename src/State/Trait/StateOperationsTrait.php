<?php

declare(strict_types=1);

namespace App\State\Trait;

use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use Doctrine\ORM\EntityManagerInterface;

trait StateOperationsTrait
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function setDatesBeforePersist($data, Operation $operation): void
    {
        if ($operation instanceof Post) {
            $data->setCreatedAt(new \DateTime());
            $data->setUpdatedAt(new \DateTime());
        }

        if ($operation instanceof Patch) {
            $data->setUpdatedAt(new \DateTime());
        }

        if ($operation instanceof Delete) {
            $data->setDeletedAt(new \DateTime());
        }
    }
}
