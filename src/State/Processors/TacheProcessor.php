<?php

namespace App\State\Processors;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Groupe;
use App\Entity\Tache;
use App\Entity\User;
use App\Service\Security\UserService;
use App\State\Trait\StateOperationsTrait;
use App\Tools\Enum\StaticDataEnum;
use App\Tools\Front\CustomMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class TacheProcessor implements ProcessorInterface
{
    use StateOperationsTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(EntityManagerInterface $entityManager, UserService $userService)
    {
        $this->entityManager = $entityManager;
        $this->userService = $userService;
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return Tache|JsonResponse
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        // Clean according to "Group" type
        $this->cleanReceivedData($data);

        // Default
        $createdById = null;
        $taskType = null;
        $taskGroup = null;

        // Création
        if ($operation instanceof Post) {
            $createdById = $data->getCreatedBy()->getId();
            $taskType = $data->getType();
            $taskGroup = $data->getGroupe();
        }
        // Mise à jour
        else {
            $previousData = $context['previous_data'];

            $createdById = $previousData->getCreatedBy()->getId();
            $taskType = $previousData->getType();

            if ($previousData->getGroupe() instanceof Groupe) {
                $taskGroup = $previousData->getGroupe();
            }
        }

        if (!$this->actionIsGranted($createdById, $taskType, $taskGroup)) {
            return new JsonResponse(CustomMessage::ACTION_FORBIDDEN, Response::HTTP_FORBIDDEN);
        }

        $this->setDatesBeforePersist($data, $operation);
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }

    private function cleanReceivedData($data): void
    {
        if ($data instanceof Tache && $data->getType() !== StaticDataEnum::TACHES_TYPES['Groupe']) {
            $data->setGroupe(null);
        }
    }

    private function actionIsGranted(int $createdById, string $taskType, ?Groupe $taskGroup): bool
    {
        return ($this->userService->isRoot() && $this->rootActionAllowed($createdById, $taskType))
            || ($this->userService->isAdmin() && $this->adminActionAllowed($createdById, $taskType, $taskGroup))
            || ($this->userService->isUser() && $this->userActionAllowed($createdById, $taskType));
    }

    private function rootActionAllowed(int $createdById, string $taskType): bool
    {
        $authenticatedUser = $this->userService->getAuthenticatedUser();

        if ($authenticatedUser instanceof User) {
            return $createdById === $authenticatedUser->getId() || $taskType !== StaticDataEnum::TACHES_TYPES['Utilisateur'];
        }

        return false;
    }

    private function adminActionAllowed(int $createdById, string $taskType, ?Groupe $taskGroup): bool
    {
        $authenticatedUser = $this->userService->getAuthenticatedUser();

        if ($authenticatedUser instanceof User) {
            return ($taskGroup && in_array($taskGroup, $authenticatedUser->getGroupes()->toArray()))
                || ($createdById === $authenticatedUser->getId() && $taskType === StaticDataEnum::TACHES_TYPES['Utilisateur']);
        }

        return false;
    }

    private function userActionAllowed($createdById, string $taskType): bool
    {
        $authenticatedUser = $this->userService->getAuthenticatedUser();
        if ($authenticatedUser instanceof User) {
            return $createdById === $authenticatedUser->getId()
                    && $taskType === StaticDataEnum::TACHES_TYPES['Utilisateur'];
        }

        return false;
    }
}
