<?php

namespace App\State\Processors;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Groupe;
use App\Entity\User;
use App\State\Trait\StateOperationsTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserProcessor implements ProcessorInterface
{
    use StateOperationsTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
    {
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return User
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        $this->setDatesBeforePersist($data, $operation);
        $this->setAutorisations($data, $data->getGroupes()->toArray());

        $previousData = $context['previous_data'];
        if ($data->getPassword()
            && ($operation instanceof Post || $previousData->getPassword() !== $data->getPassword())
        ) {
            $userRepository = $this->entityManager->getRepository(User::class);
            $hashedPassword = $this->passwordHasher->hashPassword($data, $data->getPassword());
            /* @phpstan-ignore-next-line */
            $userRepository->upgradePassword($data, $hashedPassword);
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }

    /**
     * @param array<Groupe> $groupes
     */
    private function setAutorisations(User $user, array $groupes): void
    {
        foreach ($groupes as $groupe) {
            foreach ($groupe->getAutorisations() as $autorisation) {
                if (!in_array($autorisation, $user->getAutorisations()->toArray())) {
                    $user->addAutorisation($autorisation);
                }
            }
        }
    }
}
