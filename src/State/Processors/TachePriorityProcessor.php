<?php

namespace App\State\Processors;

use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Pivot\UserParametre;
use App\Service\Security\UserService;
use App\State\Trait\StateOperationsTrait;
use App\Tools\Front\CustomMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class TachePriorityProcessor implements ProcessorInterface
{
    use StateOperationsTrait;

    public function __construct(
        private readonly UserService $userService,
        private readonly ProcessorInterface $persistProcessor,
        private readonly ProcessorInterface $removeProcessor
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return UserParametre|JsonResponse
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        // Only root user can createn update and delete tachePriorities
        if (!$this->userService->isRoot()) {
            return new JsonResponse(CustomMessage::ACTION_FORBIDDEN, Response::HTTP_FORBIDDEN);
        }

        // Delete
        if ($operation instanceof DeleteOperationInterface) {
            return $this->removeProcessor->process($data, $operation, $uriVariables, $context);
        }

        // Post & Patch
        $this->setDatesBeforePersist($data, $operation);
        $result = $this->persistProcessor->process($data, $operation, $uriVariables, $context);

        return $result;
    }
}
