<?php

namespace App\State\Processors;

use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Pivot\UserParametre;
use App\Entity\User;
use App\Service\Security\UserService;
use App\Service\UserParametre\UserParametreService;
use App\State\Trait\StateOperationsTrait;
use App\Tools\Enum\StaticDataEnum;
use App\Tools\Front\CustomMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class UserParametreProcessor implements ProcessorInterface
{
    use StateOperationsTrait;

    public function __construct(
        private readonly UserService $userService,
        private readonly UserParametreService $userParametreService,
        private readonly ProcessorInterface $persistProcessor,
        private readonly ProcessorInterface $removeProcessor,
    ) {
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     *
     * @return UserParametre|JsonResponse
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        // Previous data
        $previousData = $context['previous_data'];

        $user = $previousData->getUser();
        if (!$this->actionIsGranted($user)) {
            return new JsonResponse(CustomMessage::ACTION_FORBIDDEN, Response::HTTP_FORBIDDEN);
        }

        if ($operation instanceof DeleteOperationInterface) {
            return $this->removeProcessor->process($data, $operation, $uriVariables, $context);
        }

        $result = $this->persistProcessor->process($data, $operation, $uriVariables, $context);

        // Nombre d'éléments par page
        $parametreLibelle = $data->getParametre()->getLibelle();
        if ($result instanceof UserParametre && StaticDataEnum::PARAMETRES_ELEMENTS_PAR_PAGE === $parametreLibelle) {
            $this->userParametreService->setNbElementsByPage(intval($result->getValue()));
        }

        return $result;
    }

    private function actionIsGranted(User $dataUser): bool
    {
        return $this->userService->isRoot() || $this->actionAllowed($dataUser);
    }

    private function actionAllowed(User $dataUser): bool
    {
        // Admin
        if ($this->userService->isAdmin()) {
            return in_array($dataUser, $this->userService->getAllUsersOfMyGroups());
        }

        // User
        return $this->userService->getAuthenticatedUser()->getId() === $dataUser->getId();
    }
}
