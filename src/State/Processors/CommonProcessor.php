<?php

namespace App\State\Processors;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\State\Trait\StateOperationsTrait;
use Doctrine\ORM\EntityManagerInterface;

final class CommonProcessor implements ProcessorInterface
{
    use StateOperationsTrait;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array<mixed> $uriVariables
     * @param array<mixed> $context
     */
    public function process($data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        $this->setDatesBeforePersist($data, $operation);

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }
}
