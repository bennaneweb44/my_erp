<?php

namespace App\State\Providers;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Tache;
use App\Service\Security\UserService;
use App\Service\Tache\TacheService;
use App\State\Trait\StateOperationsTrait;
use App\Tools\Front\CustomMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class TacheProvider implements ProviderInterface
{
    use StateOperationsTrait;

    public function __construct(
        private readonly UserService $userService,
        private readonly TacheService $tacheService
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|JsonResponse|Tache
    {
        // User
        $user = $this->userService->getAuthenticatedUser();
        if (!$user) {
            return new JsonResponse(CustomMessage::BAD_REQUEST, Response::HTTP_NOT_FOUND);
        }

        // Collection
        if ($operation instanceof CollectionOperationInterface) {
            $filters = isset($context['filters']) ? $context['filters'] : [];

            return $this->tacheService->getAllowedCollection($user, $filters);
        }

        // Item
        $idTache = $uriVariables['id'];
        if (!$idTache) {
            return new JsonResponse(CustomMessage::BAD_REQUEST, Response::HTTP_NOT_FOUND);
        }

        return $this->tacheService->getAllowedItem($user, $idTache);
    }
}
