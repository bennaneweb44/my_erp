<?php

namespace App\State\Providers;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Pivot\UserParametre;
use App\Service\Security\UserService;
use App\Service\UserParametre\UserParametreService;
use App\State\Trait\StateOperationsTrait;
use App\Tools\Front\CustomMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class UserParametreProvider implements ProviderInterface
{
    use StateOperationsTrait;

    public function __construct(
        private readonly UserService $userService,
        private readonly UserParametreService $userParametreService
    ) {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|JsonResponse|UserParametre
    {
        // User
        $user = $this->userService->getAuthenticatedUser();
        if (!$user) {
            return new JsonResponse(CustomMessage::BAD_REQUEST, Response::HTTP_NOT_FOUND);
        }

        // Collection
        if ($operation instanceof CollectionOperationInterface) {
            $filters = isset($context['filters']) ? $context['filters'] : [];

            return $this->userParametreService->getAllowedCollection($user, $filters);
        }

        // Item
        $idUserParametre = $uriVariables['id'];
        if (!$idUserParametre) {
            return new JsonResponse(CustomMessage::BAD_REQUEST, Response::HTTP_NOT_FOUND);
        }

        return $this->userParametreService->getAllowedItem($user, $idUserParametre);
    }
}
