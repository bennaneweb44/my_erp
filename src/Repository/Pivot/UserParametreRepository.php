<?php

namespace App\Repository\Pivot;

use App\Entity\Pivot\UserParametre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserParametre>
 *
 * @method UserParametre|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserParametre|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserParametre[]    findAll()
 * @method UserParametre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserParametreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserParametre::class);
    }

    //    /**
    //     * @return UserParametre[] Returns an array of UserParametre objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('up')
    //            ->andWhere('up.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('up.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?UserParametre
    //    {
    //        return $this->createQueryBuilder('up')
    //            ->andWhere('up.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
