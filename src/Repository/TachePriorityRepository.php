<?php

namespace App\Repository;

use App\Entity\TachePriority;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TachePriority>
 *
 * @method TachePriority|null find($id, $lockMode = null, $lockVersion = null)
 * @method TachePriority|null findOneBy(array $criteria, array $orderBy = null)
 * @method TachePriority[]    findAll()
 * @method TachePriority[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TachePriorityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TachePriority::class);
    }

    //    /**
    //     * @return TachePriority[] Returns an array of TachePriority objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('t.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?TachePriority
    //    {
    //        return $this->createQueryBuilder('t')
    //            ->andWhere('t.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
