<?php

declare(strict_types=1);

namespace App\Service\Tache;

use App\Entity\Tache;
use App\Entity\User;
use App\Repository\TacheRepository;
use App\Service\EntryPointService;
use App\Service\Security\UserService;
use App\Service\Trait\FiltersTrait;
use App\Tools\Enum\StaticDataEnum;
use App\Tools\Front\CustomMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class TacheService extends EntryPointService
{
    use FiltersTrait;

    public function __construct(
        private readonly TacheRepository $tacheRepository,
        private readonly UserService $userService,
        private readonly RequestStack $requestStack
    ) {
    }

    public function getAllowedCollection(User $user, array $filters): array
    {
        // Nb Elements by page from current request
        $request = $this->requestStack->getMainRequest();
        $nbElementsParPage = $this->getNbElementsByPage($request);

        // Prepared query
        $alias = 't';
        $queryBuilder = $this->tacheRepository->createQueryBuilder($alias);
        $query = $queryBuilder->select();

        if ($this->userService->isRoot()) {
            $query = $query
                ->where("$alias.type <> :type OR $alias.createdBy = :createdBy")
                ->setParameter('type', StaticDataEnum::TACHES_TYPES['Utilisateur'])
                ->setParameter('createdBy', $user->getId());
        }

        if ($this->userService->isUser() || $this->userService->isAdmin()) {
            $groupesIds = array_map(fn ($item) => $item->getId(), $user->getGroupes()->toArray());

            $query = $query
                ->where("$alias.type = :typeAll")
                ->setParameter('typeAll', StaticDataEnum::TACHES_TYPES['Tout'])

                ->orWhere("$alias.type = :typeUser AND $alias.createdBy = :createdBy")
                ->setParameter('typeUser', StaticDataEnum::TACHES_TYPES['Utilisateur'])
                ->setParameter('createdBy', $user->getId())

                ->orWhere("$alias.type = :typeGroup AND $alias.groupe in (:groupesIds)")
                ->setParameter('typeGroup', StaticDataEnum::TACHES_TYPES['Groupe'])
                ->setParameter('groupesIds', implode(',', $groupesIds));
        }

        return $this->applyFiltersAndPaginate($query, $alias, $nbElementsParPage, $filters);
    }

    public function getAllowedItem(User $user, int $idTache): JsonResponse|Tache
    {
        $tache = $this->tacheRepository->findOneBy([
            'id' => $idTache,
        ]);

        if (!$tache) {
            return new JsonResponse(CustomMessage::RESOURCE_NOT_FOUND, Response::HTTP_NOT_FOUND);
        }

        if ($this->userService->isRoot()) {
            if ($tache->getType() === StaticDataEnum::TACHES_TYPES['Utilisateur'] && $tache->getCreatedBy() !== $user) {
                return new JsonResponse(CustomMessage::ACCESS_FORBIDDEN, Response::HTTP_FORBIDDEN);
            }
        }

        if ($this->userService->isAdmin() || $this->userService->isUser()) {
            if (
                $tache->getType() === StaticDataEnum::TACHES_TYPES['Utilisateur'] && $tache->getCreatedBy() !== $user
                || ($tache->getType() === StaticDataEnum::TACHES_TYPES['Groupe'] && !in_array($tache->getGroupe(), $user->getGroupes()->toArray()))
            ) {
                return new JsonResponse(CustomMessage::ACCESS_FORBIDDEN, Response::HTTP_FORBIDDEN);
            }
        }

        return $tache;
    }
}
