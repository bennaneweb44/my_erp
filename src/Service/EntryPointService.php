<?php

namespace App\Service;

use App\Tools\Enum\ApiEnum;
use Symfony\Component\HttpFoundation\Request;

class EntryPointService
{
    public function getNbElementsByPage(Request $request): int
    {
        $session = $request->getSession();

        if ($session->has('nbElementsByPage')) {
            return $session->get('nbElementsByPage');
        }

        return ApiEnum::DEFAULT_PAGINATION;
    }
}
