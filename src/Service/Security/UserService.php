<?php

declare(strict_types=1);

namespace App\Service\Security;

use App\Entity\User;
use App\Repository\Pivot\UserParametreRepository;
use App\Tools\Enum\StaticDataEnum;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserService
{
    /**
     * @var UserInterface
     */
    private $authenticatedUser;

    public function __construct(
        private readonly Security $security,
        private readonly UserParametreRepository $userParametreRepository
    ) {
        $this->authenticatedUser = $this->security->getUser();
    }

    public function getAuthenticatedUser(): ?User
    {
        return $this->authenticatedUser instanceof User ? $this->authenticatedUser : null;
    }

    /**
     * @return array<int, Collection<int, User>>
     */
    public function getAllUsersOfMyGroups(): array
    {
        $output = [];

        $authenticatedUser = $this->getAuthenticatedUser();
        foreach ($authenticatedUser->getGroupes() as $groupe) {
            $users = array_map(fn ($item) => $item, $groupe->getUsers()->toArray());
            $output = array_merge($output, $users);
        }

        return $output;
    }

    public function getUserParameters(): array
    {
        $parametres = $this->userParametreRepository->findBy(['user' => $this->getAuthenticatedUser()]);

        return $parametres;
    }

    public function isUser(): bool
    {
        return in_array(StaticDataEnum::USERS_ROLES['ROLE_USER'], $this->authenticatedUser->getRoles())
                && 1 === count($this->authenticatedUser->getRoles());
    }

    public function isRoot(): bool
    {
        return in_array(StaticDataEnum::USERS_ROLES['ROLE_ROOT'], $this->authenticatedUser->getRoles());
    }

    public function isAdmin(): bool
    {
        return in_array(StaticDataEnum::USERS_ROLES['ROLE_ADMIN'], $this->authenticatedUser->getRoles());
    }
}
