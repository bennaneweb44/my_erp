<?php

declare(strict_types=1);

namespace App\Service\Trait;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

trait FiltersTrait
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function applyFiltersAndPaginate(QueryBuilder &$query, string $alias, int $nbElementsByPage, array $filters): array
    {
        $page = null;
        foreach ($filters as $key => $value) {
            if ('page' === $key) {
                $page = $value;
                continue;
            }

            $query = $query->andWhere("$alias.$key = '$value'");
        }

        // Pageination
        if (null !== $page) {
            $offset = ($nbElementsByPage * $page) - $nbElementsByPage;
            $query = $query->setFirstResult((int) $offset)
                        ->setMaxResults((int) $nbElementsByPage);
        }

        // Root can read all parameters
        return $query->getQuery()->getResult();
    }
}
