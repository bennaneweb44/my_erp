<?php

declare(strict_types=1);

namespace App\Service\UserParametre;

use App\Entity\Pivot\UserParametre;
use App\Entity\User;
use App\Repository\Pivot\UserParametreRepository;
use App\Service\EntryPointService;
use App\Service\Security\UserService;
use App\Service\Trait\FiltersTrait;
use App\Tools\Front\CustomMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class UserParametreService extends EntryPointService
{
    use FiltersTrait;

    public function __construct(
        private readonly UserParametreRepository $userParametreRepository,
        private readonly UserService $userService,
        private readonly RequestStack $requestStack
    ) {
    }

    public function getAllowedCollection(User $user, array $filters): array
    {
        // Nb Elements by page from current request
        $request = $this->requestStack->getMainRequest();
        $nbElementsParPage = $this->getNbElementsByPage($request);

        $alias = 'up';
        $queryBuilder = $this->userParametreRepository->createQueryBuilder($alias);
        $query = $queryBuilder->select();

        // User and Admin can read their parameters ONLY
        if ($this->userService->isUser()) {
            $query = $query
                ->where("$alias.user = :user")
                ->setParameter('user', $user);
        }

        // Admin can read all parameters of his group ONLY
        if ($this->userService->isAdmin()) {
            $users = $this->userService->getAllUsersOfMyGroups();

            $query = $query
                ->where("$alias.user IN (:users)")
                ->setParameter('users', $users);
        }

        return $this->applyFiltersAndPaginate($query, $alias, $nbElementsParPage, $filters);
    }

    public function getAllowedItem(User $user, int $idTache): JsonResponse|UserParametre
    {
        $userParametre = $this->userParametreRepository->findOneBy([
            'id' => $idTache,
        ]);

        if (!$userParametre) {
            return new JsonResponse(CustomMessage::RESOURCE_NOT_FOUND, Response::HTTP_NOT_FOUND);
        }

        if ($this->userService->isRoot()
            || $userParametre->getUser()->getId() === $this->userService->getAuthenticatedUser()->getId()
        ) {
            return $userParametre;
        }

        return new JsonResponse(CustomMessage::ACCESS_FORBIDDEN, Response::HTTP_FORBIDDEN);
    }

    public function setNbElementsByPage(int $value): void
    {
        // Session from request
        $request = $this->requestStack->getMainRequest();
        $session = $request->getSession();
        $session->set('nbElementsByPage', $value);
    }
}
