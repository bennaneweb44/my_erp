<?php

namespace App\DataFixtures;

use App\Entity\Autorisation;
use App\Entity\Groupe;
use App\Entity\Parametre;
use App\Entity\Pivot\UserParametre;
use App\Entity\Tache;
use App\Entity\TachePriority;
use App\Entity\User;
use App\Tools\Enum\FixturesEnum;
use App\Tools\Enum\StaticDataEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;
    private ObjectManager $manager;
    private string $adminAvatar;
    private string $env;

    public function __construct(
        UserPasswordHasherInterface $passwordHasher,
        string $adminAvatar,
        string $env
    ) {
        $this->passwordHasher = $passwordHasher;
        $this->adminAvatar = $adminAvatar;
        $this->env = $env;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        // Users
        $users = $this->loadUsers();

        // Groupes
        $groupes = $this->loadGroupes();

        // Autorisations
        $autorisations = $this->loadAutorisations();

        // Parametres
        $parametres = $this->loadParametres();

        // Priorités des tâches
        $tachesPriorities = $this->loadTachesPriorities();

        if (in_array($this->env, ['dev', 'test'])) {
            // Users Groupes
            $this->loadUsersGroupes($users, $groupes);

            // Users Parametres
            $this->loadUsersParametres($users, $parametres);

            // Tâches
            $this->loadTaches($users, $tachesPriorities);

            // Autorisations Groupes
            $this->loadAutorisationsGroupes($autorisations, $groupes);

            // Autorisations Users
            $this->loadAutorisationsUsers($autorisations, $users);
        }

        $this->manager->flush();
    }

    private function loadUsers(): array
    {
        $output = [];

        $root = new User();
        $root->setNom('BENNANE');
        $root->setPrenom('Aziz');
        $root->setEmail(FixturesEnum::USERS_DATA['root']['email']);
        $hashedPassword = $this->passwordHasher->hashPassword($root, FixturesEnum::USERS_DATA['root']['password']);
        $root->setPassword($hashedPassword);
        $root->setEnabled(true);
        $root->setRoles(['ROLE_ROOT']);
        $root->setAvatar($this->adminAvatar);
        $root->setCreatedAt(new \DateTime());
        $root->setUpdatedAt(new \DateTime());
        $this->manager->persist($root);
        $output[] = $root;

        $admin = new User();
        $admin->setNom('ADMIN');
        $admin->setPrenom('Account');
        $admin->setEmail(FixturesEnum::USERS_DATA['admin']['email']);
        $hashedPassword = $this->passwordHasher->hashPassword($admin, FixturesEnum::USERS_DATA['admin']['password']);
        $admin->setPassword($hashedPassword);
        $admin->setEnabled(true);
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setAvatar(null);
        $admin->setCreatedAt(new \DateTime());
        $admin->setUpdatedAt(new \DateTime());
        $this->manager->persist($admin);
        $output[] = $admin;

        $user = new User();
        $user->setNom('User');
        $user->setPrenom('Account');
        $user->setEmail(FixturesEnum::USERS_DATA['user']['email']);
        $hashedPassword = $this->passwordHasher->hashPassword($user, FixturesEnum::USERS_DATA['user']['password']);
        $user->setPassword($hashedPassword);
        $user->setEnabled(true);
        $user->setRoles(['ROLE_USER']);
        $user->setAvatar(null);
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $this->manager->persist($user);
        $output[] = $user;

        $this->manager->flush();

        return $output;
    }

    private function loadGroupes(): array
    {
        $output = [];

        foreach (FixturesEnum::GROUPES_LIBELLES as $groupeLibelle) {
            $groupe = new Groupe();
            $groupe->setLibelle($groupeLibelle);
            $groupe->setCreatedAt(new \DateTime());
            $groupe->setUpdatedAt(new \DateTime());
            $this->manager->persist($groupe);
            $output[] = $groupe;
        }

        $this->manager->flush();

        return $output;
    }

    /**
     * @return array<mixed>
     */
    private function loadAutorisations(): array
    {
        $output = [];
        foreach (StaticDataEnum::ALL_ENTITIES as $entity) {
            foreach (StaticDataEnum::AUTORISATIONS_OBJECTS as $key => $value) {
                $autorisation = new Autorisation();
                $libelle = str_replace('entity', $entity, $key);
                $autorisation->setLibelle($libelle);
                $autorisation->setMethode($value['methode']);
                $route = str_replace('entity', $entity, $value['route']);
                $autorisation->setRoute($route);
                $autorisation->setCreatedAt(new \DateTime());
                $autorisation->setUpdatedAt(new \DateTime());

                $this->manager->persist($autorisation);
                $output[] = $autorisation;
            }
        }

        return $output;
    }

    private function loadParametres(): array
    {
        $output = [];
        foreach (StaticDataEnum::PARAMETRES_OBJECTS as $item) {
            $parametre = new Parametre();
            $parametre->setLibelle($item['libelle']);
            $parametre->setType($item['type']);
            $parametre->setDescription($item['description']);
            $parametre->setCreatedAt(new \DateTime());
            $parametre->setUpdatedAt(new \DateTime());

            $this->manager->persist($parametre);
            $output[] = $parametre;
        }

        return $output;
    }

    private function loadTachesPriorities(): array
    {
        $output = [];
        foreach (StaticDataEnum::TACHES_PRIORITIES_OBJECTS as $key => $value) {
            $tachePriority = new TachePriority();
            $tachePriority->setLibelle($key);
            $tachePriority->setCouleur($value['couleur']);
            $tachePriority->setNiveau($value['niveau']);
            $tachePriority->setCreatedAt(new \DateTime());
            $tachePriority->setUpdatedAt(new \DateTime());

            $this->manager->persist($tachePriority);
            $output[] = $tachePriority;
        }

        return $output;
    }

    /**
     * @param array<User>   $users
     * @param array<Groupe> $groupes
     */
    private function loadUsersGroupes(array $users, array $groupes): void
    {
        $i = 0;
        foreach ($users as $user) {
            $user->addGroupe($groupes[$i]);
            $groupes[$i]->addUser($user);

            $this->manager->persist($user);
            $this->manager->persist($groupes[$i]);
            ++$i;
        }
    }

    private function loadUsersParametres(array $users, array $parametres): void
    {
        foreach ($users as $user) {
            foreach ($parametres as $parametre) {
                $userParametre = new UserParametre();
                $userParametre->setUser($user);
                $userParametre->setParametre($parametre);
                $value = StaticDataEnum::PARAMETRES_ELEMENTS_PAR_PAGE === $parametre->getLibelle() ? '5' : 'enabled';
                $userParametre->setValue($value);
                $userParametre->setCreatedAt(new \DateTime());
                $userParametre->setUpdatedAt(new \DateTime());

                $this->manager->persist($userParametre);
            }
        }
    }

    /**
     * @param array<Autorisation> $autoisations
     * @param array<Groupe>       $groupes
     */
    private function loadAutorisationsGroupes(array $autoisations, array $groupes): void
    {
        foreach ($groupes as $groupe) {
            foreach ($autoisations as $autoisation) {
                $autoisation->addGroupe($groupe);
                $this->manager->persist($autoisation);
            }
        }
    }

    /**
     * @param array<Autorisation> $autoisations
     * @param array<User>         $users
     */
    private function loadAutorisationsUsers(array $autoisations, array $users): void
    {
        foreach ($users as $user) {
            foreach ($autoisations as $autoisation) {
                $autoisation->addUser($user);
                $this->manager->persist($autoisation);
            }
        }
    }

    /**
     * @param array<User> $users
     */
    private function loadTaches(array $users, array $tachesPriorities): void
    {
        // Tache pour chaque utilisateur
        foreach ($users as $user) {
            $i = 0;
            foreach (['Tout', 'Groupe', 'Utilisateur'] as $type) {
                $tache = new Tache();
                $tache->setType($type);
                $tache->setLibelle('Tache '.$type);
                $tache->setContenu('Contenu de la tâche '.$type);
                $tache->setFaite(false);
                $tache->setCreatedBy($user);
                $tache->setGroupe(null);
                $tache->setPriority($tachesPriorities[$i]);
                $tache->setCreatedAt(new \DateTime());
                $tache->setUpdatedAt(new \DateTime());

                // Utilisateur ne peut pas créer tâche de groupe
                if (in_array(StaticDataEnum::USERS_ROLES['ROLE_USER'], $user->getRoles())
                    && 1 === count($user->getRoles())
                    && $type === StaticDataEnum::TACHES_TYPES['Groupe']
                ) {
                    continue;
                }

                // Admin créé une tâche de groupe
                if (in_array(StaticDataEnum::USERS_ROLES['ROLE_ADMIN'], $user->getRoles())) {
                    foreach ($user->getGroupes() as $groupe) {
                        if (2 === $groupe->getId()) {
                            $tache->setGroupe($groupe);
                            break;
                        }
                    }
                }

                ++$i;
                $this->manager->persist($tache);
            }
        }
    }
}
