<?php

namespace App\EventSubscriber\Auth;

use App\Service\Security\UserService;
use App\Tools\Enum\ApiEnum;
use App\Tools\Enum\StaticDataEnum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthenticationSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly UserService $userService)
    {
    }

    /**
     * Get principals events of auth (login, logout, ...etc).
     *
     * @codeCoverageIgnore
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => [
                ['onLoginSuccess', 1],
            ],
        ];
    }

    public function onLoginSuccess(ResponseEvent $event): void
    {
        // Response
        $response = $event->getResponse();
        if (!$response->getContent() || Response::HTTP_OK !== $response->getStatusCode()) {
            return;
        }

        // Content
        $content = $response->getContent();
        if (!str_contains($content, '{"token":"')) {
            return;
        }

        // Nb éléments par page
        $nbElementsByPage = ApiEnum::DEFAULT_PAGINATION;
        $userParametres = $this->userService->getUserParameters();
        foreach ($userParametres as $up) {
            if (StaticDataEnum::PARAMETRES_ELEMENTS_PAR_PAGE === $up->getParametre()->getLibelle()) {
                $nbElementsByPage = $up->getValue();
                break;
            }
        }

        // Set "nbElementsByPage" to current session
        $request = $event->getRequest();
        $session = $request->getSession();
        $session->set('nbElementsByPage', $nbElementsByPage);
    }
}
