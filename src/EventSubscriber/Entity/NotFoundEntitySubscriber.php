<?php

namespace App\EventSubscriber\Entity;

use App\Tools\Front\CustomMessage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class NotFoundEntitySubscriber implements EventSubscriberInterface
{
    private const ROUTES_WITHOUT_EXCEPTION = [
        '/',
        '/authentication',
        '/authentication/refresh',
        '/api/logout',
    ];

    /**
     * When entity was not found.
     *
     * @codeCoverageIgnore
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['onUnprocessableEntity', 10],
            ],
        ];
    }

    public function onUnprocessableEntity(ExceptionEvent $event): void
    {
        $request = $event->getRequest();
        if (in_array($request->getRequestUri(), self::ROUTES_WITHOUT_EXCEPTION)) {
            return;
        }

        $exception = $event->getThrowable();
        if (!$exception instanceof NotFoundHttpException) {
            return;
        }

        $event->setResponse(new JsonResponse(CustomMessage::RESOURCE_NOT_FOUND));
    }
}
