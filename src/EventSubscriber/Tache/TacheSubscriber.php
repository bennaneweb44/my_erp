<?php

namespace App\EventSubscriber\Tache;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Tache;
use App\Service\Security\UserService;
use App\Tools\Enum\StaticDataEnum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class TacheSubscriber implements EventSubscriberInterface
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Notify all users of the group when new task added.
     *
     * @codeCoverageIgnore
     */
    public static function getSubscribedEvents(): array
    {
        return [
          KernelEvents::VIEW => ['sendNotification', EventPriorities::POST_WRITE],
        ];
    }

    public function sendNotification(ViewEvent $event): void
    {
        $tache = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$tache instanceof Tache || $tache->getType() !== StaticDataEnum::TACHES_TYPES['Groupe'] || Request::METHOD_POST !== $method) {
            return;
        }

        $createdBy = $this->userService->getAuthenticatedUser();

        // TODO : send notifications to users of this group
        // TODO : send notifications to users of this group
        // TODO : send notifications to users of this group
        // TODO : send notifications to users of this group
        // TODO : send notifications to users of this group
        // TODO : send notifications to users of this group
    }
}
