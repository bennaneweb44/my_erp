<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231027165205 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parametres (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE autorisations_groupes RENAME INDEX idx_962f483752c5e836 TO IDX_C77FD7FB52C5E836');
        $this->addSql('ALTER TABLE autorisations_groupes RENAME INDEX idx_962f48377a45358c TO IDX_C77FD7FB7A45358C');
        $this->addSql('ALTER TABLE taches RENAME INDEX idx_93872075b03a8386 TO IDX_3BF2CD98B03A8386');
        $this->addSql('ALTER TABLE taches RENAME INDEX idx_938720757a45358c TO IDX_3BF2CD987A45358C');
        $this->addSql('ALTER TABLE users RENAME INDEX uniq_8d93d649e7927c74 TO UNIQ_1483A5E9E7927C74');
        $this->addSql('ALTER TABLE users_groupes RENAME INDEX idx_61eb971ca76ed395 TO IDX_A7631213A76ED395');
        $this->addSql('ALTER TABLE users_groupes RENAME INDEX idx_61eb971c7a45358c TO IDX_A76312137A45358C');
        $this->addSql('ALTER TABLE users_autorisations RENAME INDEX idx_89684d36a76ed395 TO IDX_33569782A76ED395');
        $this->addSql('ALTER TABLE users_autorisations RENAME INDEX idx_89684d3652c5e836 TO IDX_3356978252C5E836');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE parametres');
        $this->addSql('ALTER TABLE users_groupes RENAME INDEX idx_a7631213a76ed395 TO IDX_61EB971CA76ED395');
        $this->addSql('ALTER TABLE users_groupes RENAME INDEX idx_a76312137a45358c TO IDX_61EB971C7A45358C');
        $this->addSql('ALTER TABLE taches RENAME INDEX idx_3bf2cd98b03a8386 TO IDX_93872075B03A8386');
        $this->addSql('ALTER TABLE taches RENAME INDEX idx_3bf2cd987a45358c TO IDX_938720757A45358C');
        $this->addSql('ALTER TABLE users_autorisations RENAME INDEX idx_33569782a76ed395 TO IDX_89684D36A76ED395');
        $this->addSql('ALTER TABLE users_autorisations RENAME INDEX idx_3356978252c5e836 TO IDX_89684D3652C5E836');
        $this->addSql('ALTER TABLE users RENAME INDEX uniq_1483a5e9e7927c74 TO UNIQ_8D93D649E7927C74');
        $this->addSql('ALTER TABLE autorisations_groupes RENAME INDEX idx_c77fd7fb52c5e836 TO IDX_962F483752C5E836');
        $this->addSql('ALTER TABLE autorisations_groupes RENAME INDEX idx_c77fd7fb7a45358c TO IDX_962F48377A45358C');
    }
}
