<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231101022033 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taches ADD priority_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE taches ADD CONSTRAINT FK_3BF2CD98497B19F9 FOREIGN KEY (priority_id) REFERENCES tache_priorities (id)');
        $this->addSql('CREATE INDEX IDX_3BF2CD98497B19F9 ON taches (priority_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taches DROP FOREIGN KEY FK_3BF2CD98497B19F9');
        $this->addSql('DROP INDEX IDX_3BF2CD98497B19F9 ON taches');
        $this->addSql('ALTER TABLE taches DROP priority_id');
    }
}
