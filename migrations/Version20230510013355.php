<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230510013355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE autorisations (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, methode VARCHAR(10) NOT NULL, route VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE autorisations_groupes (autorisation_id INT NOT NULL, groupe_id INT NOT NULL, INDEX IDX_962F483752C5E836 (autorisation_id), INDEX IDX_962F48377A45358C (groupe_id), PRIMARY KEY(autorisation_id, groupe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE autorisations_groupes ADD CONSTRAINT FK_962F483752C5E836 FOREIGN KEY (autorisation_id) REFERENCES autorisations (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE autorisations_groupes ADD CONSTRAINT FK_962F48377A45358C FOREIGN KEY (groupe_id) REFERENCES groupes (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE autorisations_groupes DROP FOREIGN KEY FK_962F483752C5E836');
        $this->addSql('ALTER TABLE autorisations_groupes DROP FOREIGN KEY FK_962F48377A45358C');
        $this->addSql('DROP TABLE autorisations');
        $this->addSql('DROP TABLE autorisations_groupes');
    }
}
