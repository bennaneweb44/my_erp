<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230524012509 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE users_autorisations (user_id INT NOT NULL, autorisation_id INT NOT NULL, INDEX IDX_89684D36A76ED395 (user_id), INDEX IDX_89684D3652C5E836 (autorisation_id), PRIMARY KEY(user_id, autorisation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_autorisations ADD CONSTRAINT FK_89684D36A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_autorisations ADD CONSTRAINT FK_89684D3652C5E836 FOREIGN KEY (autorisation_id) REFERENCES autorisations (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users_autorisations DROP FOREIGN KEY FK_89684D36A76ED395');
        $this->addSql('ALTER TABLE users_autorisations DROP FOREIGN KEY FK_89684D3652C5E836');
        $this->addSql('DROP TABLE users_autorisations');
    }
}
