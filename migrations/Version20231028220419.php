<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231028220419 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE users_parametres (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, parametre_id INT NOT NULL, value VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_CD12D609A76ED395 (user_id), INDEX IDX_CD12D6096358FF62 (parametre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_parametres ADD CONSTRAINT FK_CD12D609A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users_parametres ADD CONSTRAINT FK_CD12D6096358FF62 FOREIGN KEY (parametre_id) REFERENCES parametres (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users_parametres DROP FOREIGN KEY FK_CD12D609A76ED395');
        $this->addSql('ALTER TABLE users_parametres DROP FOREIGN KEY FK_CD12D6096358FF62');
        $this->addSql('DROP TABLE users_parametres');
    }
}
