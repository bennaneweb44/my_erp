<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230927194822 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE taches (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, groupe_id INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, contenu LONGTEXT NOT NULL, type VARCHAR(30) NOT NULL, faite TINYINT(1) DEFAULT NULL, INDEX IDX_93872075B03A8386 (created_by_id), INDEX IDX_938720757A45358C (groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE taches ADD CONSTRAINT FK_93872075B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE taches ADD CONSTRAINT FK_938720757A45358C FOREIGN KEY (groupe_id) REFERENCES groupes (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taches DROP FOREIGN KEY FK_93872075B03A8386');
        $this->addSql('ALTER TABLE taches DROP FOREIGN KEY FK_938720757A45358C');
        $this->addSql('DROP TABLE taches');
    }
}
