<?php

namespace App\Tests\Functional\User;

use App\Entity\Groupe;
use App\Entity\User;
use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\ApiEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserTest extends FunctionalTestCase
{
    protected array $options = [];
    protected const URL = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_USERS;

    public function testGetAll(): void
    {
        $response = $this->createClientWithCredentials()->request(Request::METHOD_GET, self::URL);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testPostUser(): int
    {
        $this->options['json'] = [
            'nom' => 'jean',
            'prenom' => 'doe',
            'email' => 'jean@email.com',
            'roles' => [
                'ROLE_USER',
            ],
            'password' => 'password',
            'raisonSociale' => null,
            'avatar' => null,
            'enabled' => false,
            'groupes' => [
                '/api/groupes/6',
            ],
        ];

        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL, $this->options);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $arrayResponse = json_decode($response->getContent(), true);
        $this->assertIsArray($arrayResponse);
        $this->assertArrayHasKey('id', $arrayResponse);

        return $arrayResponse['id'];
    }

    /**
     * @depends testPostUser
     */
    public function testUserGroups(int $insertedId): int
    {
        // User
        $response = $this->createClientWithCredentials()->request(Request::METHOD_GET, self::URL.'/'.$insertedId);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $arrayResponse = json_decode($response->getContent(), true);
        $this->assertIsArray($arrayResponse);
        $this->assertArrayHasKey('groupes', $arrayResponse);
        $this->assertCount(1, $arrayResponse['groupes']);

        // Group
        $groupeObject = $this->entityManager->getRepository(Groupe::class)->find(7);
        $userObject = $this->entityManager->getRepository(User::class)->find($insertedId);

        // Add Group to User
        $groupeObject->addUser($userObject);
        $this->assertCount(2, $userObject->getGroupes());

        // Remove Group from User
        $groupeObject->removeUser($userObject);
        $this->assertCount(1, $userObject->getGroupes());

        return $insertedId;
    }

    /**
     * @depends testUserGroups
     */
    public function testDeleteUser(int $insertedId): void
    {
        $response = $this->createClientWithCredentials()->request('DELETE', self::URL.'/'.$insertedId, $this->options);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function testBadLogin(): void
    {
        $response = $this->request(Request::METHOD_POST, self::URL, [
            'json' => [
                'username' => 'bad@bad.com',
                'password' => 'bad',
            ],
        ]);

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }
}
