<?php

namespace App\Tests\Functional\Groupe;

use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\ApiEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupeTest extends FunctionalTestCase
{
    protected array $options = [];
    protected const URL = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_GROUPES;

    public function testGetAll(): void
    {
        $response = $this->createClientWithCredentials()->request(Request::METHOD_GET, self::URL);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testPostGroupe(): int
    {
        $this->options['json'] = [
            'libelle' => 'libelleGroupe1',
        ];

        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL, $this->options);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $arrayResponse = json_decode($response->getContent(), true);
        $this->assertIsArray($arrayResponse);
        $this->assertArrayHasKey('id', $arrayResponse);

        return $arrayResponse['id'];
    }

    /**
     * @depends testPostGroupe
     */
    public function testPutAndDeleteGroupe(int $insertedId): void
    {
        $this->options['headers'] = [
            'Content-Type' => 'application/merge-patch+json',
        ];
        $this->options['json'] = [
            'libelle' => 'libelleGroupeUpdated',
            'user' => '/api/users/1',
        ];

        $response = $this->createClientWithCredentials()->request(Request::METHOD_PATCH, self::URL.'/'.$insertedId, $this->options);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $arrayResponse = json_decode($response->getContent(), true);
        $this->assertIsArray($arrayResponse);
        $this->assertArrayHasKey('id', $arrayResponse);

        $response = $this->createClientWithCredentials()->request('DELETE', self::URL.'/'.$insertedId, $this->options);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}
