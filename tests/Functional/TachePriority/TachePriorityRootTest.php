<?php

namespace App\Tests\Functional\TachePriority;

use ApiPlatform\Symfony\Bundle\Test\Client;
use ApiPlatform\Symfony\Validator\Exception\ValidationException;
use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\StaticDataEnum;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TachePriorityRootTest extends FunctionalTestCase
{
    private Client $clientRoot;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientRoot = $this->signInUser('root');
    }

    public function testReadAll(): void
    {
        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_TACHE_PRIORITIES, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertIsArray($content);
        $this->assertNotEmpty($content);
        $item = $content[0];
        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('libelle', $item);
        $this->assertArrayHasKey('couleur', $item);
        $this->assertArrayHasKey('niveau', $item);
    }

    public function testReadLastItem(): void
    {
        $lastId = count(StaticDataEnum::TACHES_PRIORITIES_OBJECTS);
        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_TACHE_PRIORITIES.'/'.$lastId, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertIsArray($content);
        $this->assertNotEmpty($content);
        $this->assertArrayHasKey('id', $content);
        $this->assertArrayHasKey('libelle', $content);
        $this->assertArrayHasKey('couleur', $content);
        $this->assertArrayHasKey('niveau', $content);
    }

    /**
     * @dataProvider postDataProvider
     */
    public function testPostNewItem(array $data, int $assert): void
    {
        if (Response::HTTP_UNPROCESSABLE_ENTITY === $assert) {
            try {
                $response = $this->clientRoot->request(Request::METHOD_POST, self::URL_TACHE_PRIORITIES, $data);
            } catch (ClientException $ex) {
                $this->expectException(ValidationException::class);
                $this->expectExceptionMessage($ex->getMessage());
            }
        } elseif (Response::HTTP_CREATED === $assert) {
            $response = $this->clientRoot->request(Request::METHOD_POST, self::URL_TACHE_PRIORITIES, $data);
            $this->assertEquals($assert, $response->getStatusCode());
        }
    }

    /**
     * @dataProvider patchDataProvider
     */
    public function testPatchExistingItem(int $id, array $json, int $assert): void
    {
        $patch = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => $json,
        ];

        $response = $this->clientRoot->request(Request::METHOD_PATCH, self::URL_TACHE_PRIORITIES.'/'.$id, $patch);
        $this->assertEquals($assert, $response->getStatusCode());
    }

    public function testDeleteItem(): void
    {
        $delete = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ];

        // Last item deleted
        $lastId = count(StaticDataEnum::TACHES_PRIORITIES_OBJECTS) + 1;
        $itemDeleted = $this->clientRoot->request(Request::METHOD_DELETE, self::URL_TACHE_PRIORITIES.'/'.$lastId, $delete);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $itemDeleted->getStatusCode());

        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_TACHE_PRIORITIES.'/'.$lastId, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public static function postDataProvider(): array
    {
        return [
            'can_post_new_niveau' => [
                'data' => [
                    'json' => [
                        'libelle' => 'libelle 1',
                        'couleur' => 'couleur 1',
                        'niveau' => count(StaticDataEnum::TACHES_PRIORITIES_OBJECTS) + 1,
                    ],
                ],
                'assert' => Response::HTTP_CREATED,
            ],
            'cant_post_existing_niveau' => [
                'data' => [
                    'json' => [
                        'libelle' => 'libelle 2',
                        'couleur' => 'couleur 2',
                        'niveau' => count(StaticDataEnum::TACHES_PRIORITIES_OBJECTS),
                    ],
                ],
                'assert' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ],
        ];
    }

    public static function patchDataProvider(): array
    {
        return [
            'can_patch_without_modify_niveau' => [
                'id' => 1,
                'json' => [
                    'libelle' => 'new libelle 1',
                    'couleur' => 'new couleur 1',
                ],
                'assert' => Response::HTTP_OK,
            ],
            'cant_patch_with_existing_niveau' => [
                'id' => 2,
                'json' => [
                    'libelle' => 'new libelle 2',
                    'couleur' => 'new couleur 2',
                    'niveau' => 1,
                ],
                'assert' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ],
        ];
    }
}
