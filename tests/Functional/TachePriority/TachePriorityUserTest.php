<?php

namespace App\Tests\Functional\TachePriority;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\StaticDataEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TachePriorityUserTest extends FunctionalTestCase
{
    private Client $clientUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientUser = $this->signInUser('user');
    }

    public function testReadAll(): void
    {
        $response = $this->clientUser->request(Request::METHOD_GET, self::URL_TACHE_PRIORITIES, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertIsArray($content);
        $this->assertNotEmpty($content);
        $item = $content[0];
        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('libelle', $item);
        $this->assertArrayHasKey('couleur', $item);
        $this->assertArrayHasKey('niveau', $item);
    }

    public function testReadLastItem(): void
    {
        $lastId = count(StaticDataEnum::TACHES_PRIORITIES_OBJECTS);
        $response = $this->clientUser->request(Request::METHOD_GET, self::URL_TACHE_PRIORITIES.'/'.$lastId, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertIsArray($content);
        $this->assertNotEmpty($content);
        $this->assertArrayHasKey('id', $content);
        $this->assertArrayHasKey('libelle', $content);
        $this->assertArrayHasKey('couleur', $content);
        $this->assertArrayHasKey('niveau', $content);
    }

    public function testPostNewItem(): void
    {
        $data = [
            'json' => [
                'libelle' => 'libelle 1',
                'couleur' => 'couleur 1',
                'niveau' => 99,
            ],
        ];

        $response = $this->clientUser->request(Request::METHOD_POST, self::URL_TACHE_PRIORITIES, $data);
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testPatchExistingItem(): void
    {
        $lastId = count(StaticDataEnum::TACHES_PRIORITIES_OBJECTS);
        $patch = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'libelle' => 'new libelle',
            ],
        ];

        $response = $this->clientUser->request(Request::METHOD_PATCH, self::URL_TACHE_PRIORITIES.'/'.$lastId, $patch);
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    public function testDeleteItem(): void
    {
        $delete = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ];

        // Last item cant be deleted by Admin
        $lastId = count(StaticDataEnum::TACHES_PRIORITIES_OBJECTS);
        $itemDeleted = $this->clientUser->request(Request::METHOD_DELETE, self::URL_TACHE_PRIORITIES.'/'.$lastId, $delete);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $itemDeleted->getStatusCode());

        $response = $this->clientUser->request(Request::METHOD_GET, self::URL_TACHE_PRIORITIES.'/'.$lastId, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
