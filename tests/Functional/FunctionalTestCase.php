<?php

namespace App\Tests\Functional;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Entity\User;
use App\Tools\Enum\ApiEnum;
use App\Tools\Enum\FixturesEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class FunctionalTestCase extends ApiTestCase
{
    protected const URL_TACHES = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_TACHES;
    protected const URL_TACHE_PRIORITIES = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_TACHE_PRIORITIES;
    protected const URL_USERS = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_USERS;
    protected const URL_GROUPES = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_GROUPES;
    protected const URL_USER_PARAMETRES = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_USER_PARAMETRES;

    protected ?string $token = null;
    protected $entityManager;
    protected User $currentUser;

    public function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = static::createClient()->getContainer()->get('doctrine')->getManager();
    }

    protected function createClientWithCredentials($credentials = null): Client
    {
        if ($credentials) {
            $this->currentUser = $this->entityManager->getRepository(User::class)->findOneBy([
                'email' => $credentials['username'],
            ]);
        }

        $token = $this->getToken($credentials);

        $client = static::createClient([], [
            'headers' => [
                'authorization' => 'Bearer '.$token,
                ],
            ]
        );

        return $client;
    }

    /**
     * Use other credentials if needed.
     */
    protected function getToken($body = null): string
    {
        if ($this->token) {
            return $this->token;
        }

        $response = static::createClient()->request(Request::METHOD_POST, '/authentication', ['json' => $body ?: [
            'username' => FixturesEnum::USERS_DATA['root']['email'],
            'password' => FixturesEnum::USERS_DATA['root']['password'],
        ]]);

        $this->assertResponseIsSuccessful();
        $data = $response->toArray();
        $this->assertArrayHasKey('token', $data);
        $this->assertIsString($data['token']);
        $this->token = $data['token'];

        return $data['token'];
    }

    /**
     * @param array<string, mixed> $options
     */
    protected function request(string $method, string $url, array $options = []): ResponseInterface
    {
        $client = static::createClient();

        return $client->request($method, $url, $options);
    }

    protected function signInUser(string $type): Client
    {
        $credentials = null;

        switch ($type) {
            case 'root':
                $credentials = [
                    'username' => FixturesEnum::USERS_DATA['root']['email'],
                    'password' => FixturesEnum::USERS_DATA['root']['password'],
                ];
                break;
            case 'admin':
                $credentials = [
                    'username' => FixturesEnum::USERS_DATA['admin']['email'],
                    'password' => FixturesEnum::USERS_DATA['admin']['password'],
                ];
                break;
            case 'user':
                $credentials = [
                    'username' => FixturesEnum::USERS_DATA['user']['email'],
                    'password' => FixturesEnum::USERS_DATA['user']['password'],
                ];
                break;
        }

        return $this->createClientWithCredentials($credentials);
    }
}
