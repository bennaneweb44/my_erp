<?php

namespace App\Tests\Functional\UserParametre;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Tests\Functional\FunctionalTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserParametreUserTest extends FunctionalTestCase
{
    private Client $clientUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientUser = $this->signInUser('user');
    }

    public function testReadAll(): void
    {
        $response = $this->clientUser->request(Request::METHOD_GET, self::URL_USER_PARAMETRES, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertIsArray($content);
        $this->assertNotEmpty($content);
        $item = $content[0];
        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('user', $item);
        $this->assertArrayHasKey('parametre', $item);
        $this->assertArrayHasKey('value', $item);
    }

    /**
     * @dataProvider patchAllDataProvider
     */
    public function testPatchAll(int $id, int $assert): void
    {
        $patch = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'value' => 'new value',
            ],
        ];

        $response = $this->clientUser->request(Request::METHOD_PATCH, self::URL_USER_PARAMETRES.'/'.$id, $patch);
        $this->assertEquals($assert, $response->getStatusCode());
    }

    public static function patchAllDataProvider(): array
    {
        return [
            'root_parametre' => [
                'id' => 2,
                'assert' => Response::HTTP_FORBIDDEN,
            ],
            'admin_parametre' => [
                'id' => 3,
                'assert' => Response::HTTP_FORBIDDEN,
            ],
            'user_parametre' => [
                'id' => 5,
                'assert' => Response::HTTP_OK,
            ],
        ];
    }
}
