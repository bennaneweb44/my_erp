<?php

namespace App\Tests\Functional\UserParametre;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\StaticDataEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\ResponseInterface;

class UserParametreRootTest extends FunctionalTestCase
{
    private Client $clientRoot;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientRoot = $this->signInUser('root');
    }

    public function testReadAll(): void
    {
        // First page
        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_USER_PARAMETRES.'?page=1', [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertIsArray($content);
        $this->assertNotEmpty($content);
        $item = $content[0];
        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('user', $item);
        $this->assertArrayHasKey('parametre', $item);
        $this->assertArrayHasKey('value', $item);

        // Get id of "NbElementByPage" parameter
        $id = 0;
        foreach ($content as $c) {
            if (StaticDataEnum::PARAMETRES_ELEMENTS_PAR_PAGE === $c['parametre']['libelle']) {
                $id = $c['id'];
                break;
            }
        }

        /**
         * Seconde page is empty => default NbElementsByPage = 30.
         */
        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_USER_PARAMETRES.'?page=2', [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertIsArray($content);
        $this->assertEmpty($content);

        /*
         * New "nbElementsByPage"
         */
        $this->patchNewValueRequest($id, '3');
        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_USER_PARAMETRES.'?page=2', [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        $this->assertIsArray($content);
        $this->assertNotEmpty($content);
        $this->assertCount(3, $content);
        $item = $content[0];
        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('user', $item);
        $this->assertArrayHasKey('parametre', $item);
        $this->assertArrayHasKey('value', $item);
    }

    /**
     * @dataProvider patchAllDataProvider
     */
    public function testPatchAll(int $id, int $assert): void
    {
        $response = $this->patchNewValueRequest($id, 'newValue');
        $this->assertEquals($assert, $response->getStatusCode());
    }

    public function testDeleteItem(): void
    {
        $delete = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
        ];

        // Item deleted
        $itemDeleted = $this->clientRoot->request(Request::METHOD_DELETE, self::URL_USER_PARAMETRES.'/1', $delete);
        $this->assertEquals(Response::HTTP_NO_CONTENT, $itemDeleted->getStatusCode());

        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_USER_PARAMETRES.'/1', [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public static function patchAllDataProvider(): array
    {
        return [
            'root_parametre' => [
                'id' => 1,
                'assert' => Response::HTTP_OK,
            ],
            'admin_parametre' => [
                'id' => 3,
                'assert' => Response::HTTP_OK,
            ],
            'user_parametre' => [
                'id' => 5,
                'assert' => Response::HTTP_OK,
            ],
        ];
    }

    private function patchNewValueRequest(int $id, string $value): ResponseInterface
    {
        $patch = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'value' => $value,
            ],
        ];

        $response = $this->clientRoot->request(Request::METHOD_PATCH, self::URL_USER_PARAMETRES.'/'.$id, $patch);

        return $response;
    }
}
