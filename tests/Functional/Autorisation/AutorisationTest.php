<?php

namespace App\Tests\Functional\Autorisation;

use App\Entity\Autorisation;
use App\Entity\User;
use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\ApiEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AutorisationTest extends FunctionalTestCase
{
    protected array $options = [];
    protected const URL_USER = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_USERS;
    protected const URL_GROUPE = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_GROUPES;
    protected const URL_AUTORISATION = '/'.ApiEnum::URI_PREFIX.'/'.ApiEnum::API_AUTORISATIONS;

    /**
     * Scenario :.
     *
     *   1- créer un groupe
     *   2- créer une autorisation
     *   3- lui attribuer cette autorisation
     *   4- créer un user
     *   5- lui attribuer ce groupe
     *   6- vérifier que le user a bien cette autorisation
     *   7- créer un autre groupe
     *   8- créer une autre autorisation
     *   9- attribuer cette 2eme autorisation au 2eme groupe
     *  10- attribuer ce 2eme groupe au même user
     *  11- vérifier que le user a bien cette 2eme autorisation
     */
    public function testThisScenario(): int
    {
        // 1- créer un groupe
        $groupeJson = [
            'json' => [
                'libelle' => 'test_groupe',
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL_GROUPE, $groupeJson);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $groupeIri_1 = $object['@id'];

        // 2- créer une autorisation
        $autorisation = [
            'json' => [
                'libelle' => 'tests_get_all',
                'methode' => 'get',
                'route' => '/tests',
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL_AUTORISATION, $autorisation);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $this->assertArrayHasKey('id', $object);
        $idAutorisation = $object['id'];
        $this->assertIsInt($idAutorisation);
        $autorisationIri_1 = $object['@id'];

        // 3- lui attribuer cette autorisation
        $groupeToAutorisation = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'autorisations' => [
                    $autorisationIri_1,
                ],
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_PATCH, $groupeIri_1, $groupeToAutorisation);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // 4- créer un user
        $user = [
            'json' => [
                'nom' => 'newNom',
                'prenom' => 'newPrenom',
                'email' => 'newemail@email.com',
                'roles' => [
                    'ROLE_USER',
                ],
                'password' => 'password',
                'raisonSociale' => null,
                'avatar' => null,
                'enabled' => false,
                'groupes' => [
                    '/api/groupes/1',
                ],
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL_USER, $user);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $userIri = $object['@id'];

        // 5- lui attribuer ce groupe
        $groupeToUser = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'groupes' => [
                    $groupeIri_1,
                ],
            ],
        ];

        $response = $this->createClientWithCredentials()->request(Request::METHOD_PATCH, $userIri, $groupeToUser);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // 6- vérifier que le user a bien cette autorisation
        $response = $this->createClientWithCredentials()->request(Request::METHOD_GET, $userIri);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $userHasThisAutorization = false;
        foreach ($object['autorisations'] as $userAut) {
            if ($autorisationIri_1 === $userAut['@id']) {
                $userHasThisAutorization = true;
                break;
            }
        }
        $this->assertTrue($userHasThisAutorization);

        // 7- créer un autre groupe
        $groupeJson = [
            'json' => [
                'libelle' => 'test_groupe_2',
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL_GROUPE, $groupeJson);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $groupeIri_2 = $object['@id'];

        // 8- créer une autre autorisation
        $autorisation = [
            'json' => [
                'libelle' => 'tests_get_all_2',
                'methode' => 'get',
                'route' => '/tests-2',
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL_AUTORISATION, $autorisation);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $autorisationIri_2 = $object['@id'];

        // 9- attribuer cette 2eme autorisation au 2eme groupe
        $autorisationAuDeuxiemeGroupe = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'autorisations' => [
                    $autorisationIri_2,
                ],
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_PATCH, $groupeIri_2, $autorisationAuDeuxiemeGroupe);

        // 10- attribuer ce 2eme groupe au même user
        $groupeToUser = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'groupes' => [
                    $groupeIri_2,
                ],
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_PATCH, $userIri, $groupeToUser);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // 11- vérifier que le user a bien cette 2eme autorisation
        $response = $this->createClientWithCredentials()->request(Request::METHOD_GET, $userIri);
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $userHasThisAutorization = false;
        foreach ($object['autorisations'] as $userAut) {
            if ($autorisationIri_2 === $userAut['@id']) {
                $userHasThisAutorization = true;
                break;
            }
        }
        $this->assertTrue($userHasThisAutorization);

        return $idAutorisation;
    }

    /**
     * @depends testThisScenario
     */
    public function testUsersMethods(int $idAutorisation): void
    {
        // Get all
        $autorisation = $this->entityManager->getRepository(Autorisation::class)->findOneBy(['id' => $idAutorisation]);
        $this->assertNotNull($autorisation);
        $this->assertInstanceOf(Autorisation::class, $autorisation);
        $this->assertIsIterable($autorisation->getUsers());
        $nbUsers = 1;
        $this->assertCount($nbUsers, $autorisation->getUsers());

        // New user
        $user = [
            'json' => [
                'nom' => 'newNom2',
                'prenom' => 'newPrenom2',
                'email' => 'newemail2@email.com',
                'roles' => [
                    'ROLE_USER',
                ],
                'password' => 'password',
                'raisonSociale' => null,
                'avatar' => null,
                'enabled' => false,
                'groupes' => [
                    '/api/groupes/1',
                ],
            ],
        ];
        $response = $this->createClientWithCredentials()->request(Request::METHOD_POST, self::URL_USER, $user);
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
        $object = json_decode($response->getcontent(), true);
        $this->assertIsArray($object);
        $this->assertArrayHasKey('id', $object);
        $idUser = $object['id'];

        // Add user to autorisation
        $userEntity = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $idUser]);
        $this->assertInstanceOf(User::class, $userEntity);
        $autorisation->addUser($userEntity);
        $this->assertCount(++$nbUsers, $autorisation->getUsers());

        // Remove user
        $autorisation->removeUser($userEntity);
        $this->assertCount(--$nbUsers, $autorisation->getUsers());

        // Get groupes
        $groupes = $autorisation->getGroupes();
        $this->assertIsIterable($groupes);
        $nbGroupes = 1;
        $this->assertCount($nbGroupes, $autorisation->getGroupes());

        // Remove groupe
        $groupe = $userEntity->getGroupes()[0];
        $this->assertCount(1, $userEntity->getGroupes());
        $userEntity->removeGroupe($groupe);
        $this->assertEmpty($userEntity->getGroupes());

        $groupe->removeUser($userEntity);
        foreach ($groupe->getUsers() as $usr) {
            $this->assertNotEquals($usr->getId(), $userEntity->getId());
        }

        $this->assertNotEmpty($groupe->getAutorisations());
        foreach ($groupe->getAutorisations() as $autor) {
            $groupe->removeAutorisation($autor);
        }
        $this->assertEmpty($groupe->getAutorisations());
    }
}
