<?php

namespace App\Tests\Functional\Tache;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\StaticDataEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TacheAdminTest extends FunctionalTestCase
{
    protected array $options = [];
    private Client $clientAdmin;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientAdmin = $this->signInUser('admin');
    }

    /**
     * @dataProvider writeUserTypeTaskDataProvider
     * @dataProvider writeGroupTypeTaskDataProvider
     * @dataProvider writeAllTypeTaskDataProvider
     */
    public function testWriteAdminTache(array $data, array $assert): void
    {
        $this->token = '';
        $response = $this->clientAdmin->request(Request::METHOD_POST, self::URL_TACHES, ['json' => $data]);

        if (Response::HTTP_CREATED === $assert['codeResponse']) {
            $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
            $item = json_decode($response->getContent(), true);
            $response = $this->clientAdmin->request(Request::METHOD_GET, $item['@id'], [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]);
            $item = json_decode($response->getContent(), true);
            $this->assertEquals($item['type'], $data['type']);
            $this->assertEquals($item['createdBy'], $data['createdBy']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    /**
     * @dataProvider getAllTasksDataProvider
     */
    public function testGetAll(string $filterName, string $filterValue): void
    {
        $response = $this->clientAdmin->request(Request::METHOD_GET, self::URL_TACHES."?$filterName=$filterValue", [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $arrayResponse = json_decode($response->getContent(), true);
        foreach ($arrayResponse as $item) {
            switch ($filterName) {
                case StaticDataEnum::TACHES_TYPES['Tout']:
                    $this->assertNull($item['groupe']);
                    break;
                case StaticDataEnum::TACHES_TYPES['Groupe']:
                    $this->assertNotNull($item['groupe']);
                    $groupesUris = array_map(fn ($item) => self::URL_GROUPES.'/'.$item->getId(), $this->currentUser->getGroupes()->toArray());
                    $this->assertContains($item['groupe'], $groupesUris);
                    break;
                case StaticDataEnum::TACHES_TYPES['Utilisateur']:
                    $this->assertEquals($item['createdBy'], self::URL_USERS.'/'.$this->currentUser->getId());
                    $this->assertNull($item['groupe']);
                    break;
            }
        }
    }

    // Provider get All
    public static function getAllTasksDataProvider(): array
    {
        $created_200 = [
            'filter_tout' => [
                'filterName' => 'type',
                'filterValue' => StaticDataEnum::TACHES_TYPES['Tout'],
            ],
            'filter_groupe' => [
                'filterName' => 'type',
                'filterValue' => StaticDataEnum::TACHES_TYPES['Groupe'],
            ],
            'filter_utilisateur' => [
                'filterName' => 'type',
                'filterValue' => StaticDataEnum::TACHES_TYPES['Utilisateur'],
            ],
        ];

        return $created_200;
    }

    // Provider for User Type Task
    public static function writeUserTypeTaskDataProvider(): array
    {
        $created_201 = [
            'admin_can_create_his_own' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Utilisateur',
                    'faite' => false,
                    'createdBy' => '/api/users/2',
                    'priority' => '/api/tache_priorities/1',
                    'groupe' => null,
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_CREATED,
                ],
            ],
        ];

        $forbidden_403 = [
            'admin_cant_create_for_another_user' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Utilisateur',
                    'faite' => false,
                    'createdBy' => '/api/users/3',
                    'priority' => '/api/tache_priorities/1',
                    'groupe' => null,
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_FORBIDDEN,
                ],
            ],
        ];

        return array_merge($created_201, $forbidden_403);
    }

    // Provider for Group Type Task
    public static function writeGroupTypeTaskDataProvider(): array
    {
        $created_201 = [
            'admin_can_create_for_his_group' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Groupe',
                    'faite' => false,
                    'createdBy' => '/api/users/2',
                    'priority' => '/api/tache_priorities/2',
                    'groupe' => '/api/groupes/2',
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_CREATED,
                ],
            ],
        ];

        $forbidden_403 = [
            'admin_cant_create_for_another_group' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Groupe',
                    'faite' => false,
                    'createdBy' => '/api/users/2',
                    'priority' => '/api/tache_priorities/2',
                    'groupe' => '/api/groupes/3',
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_FORBIDDEN,
                ],
            ],
        ];

        return array_merge($created_201, $forbidden_403);
    }

    // Provider for All Type Task
    public static function writeAllTypeTaskDataProvider(): array
    {
        $created_403 = [
            'admin_cant_create_for_all_users' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Tout',
                    'faite' => false,
                    'createdBy' => '/api/users/2',
                    'priority' => '/api/tache_priorities/2',
                    'groupe' => null,
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_FORBIDDEN,
                ],
            ],
        ];

        return $created_403;
    }
}
