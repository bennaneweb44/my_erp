<?php

namespace App\Tests\Functional\Tache;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Tests\Functional\FunctionalTestCase;
use App\Tools\Enum\StaticDataEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TacheRootTest extends FunctionalTestCase
{
    private Client $clientRoot;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientRoot = $this->signInUser('root');
    }

    /**
     * @dataProvider writeUserTypeTaskDataProvider
     * @dataProvider writeGroupTypeTaskDataProvider
     * @dataProvider writeAllTypeTaskDataProvider
     */
    public function testWriteAndReadItemForRootTache(array $data, array $assert): void
    {
        $response = $this->clientRoot->request(Request::METHOD_POST, self::URL_TACHES, ['json' => $data]);

        if (Response::HTTP_CREATED === $assert['codeResponse']) {
            $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
            $item = json_decode($response->getContent(), true);

            // Good response
            $response = $this->clientRoot->request(Request::METHOD_GET, $item['@id'], [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]);
            $this->assertResponseIsSuccessful();
            $item = json_decode($response->getContent(), true);
            $this->assertEquals($item['type'], $data['type']);
            $this->assertEquals($item['createdBy'], $data['createdBy']);
        } else {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        }
    }

    public function testBadResponseItemTache(): void
    {
        // Bad response
        $response = $this->clientRoot->request(Request::METHOD_GET, '/api/taches/999999', [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function testPatchItem(): void
    {
        $patch = [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
            ],
            'json' => [
                'libelle' => 'New libelle',
                'groupe' => '/api/groupes/1',
            ],
        ];

        $response = $this->clientRoot->request(Request::METHOD_PATCH, self::URL_TACHES.'/1', $patch);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @dataProvider getAllTasksDataProvider
     */
    public function testGetAll(string $filterName, string $filterValue): void
    {
        $response = $this->clientRoot->request(Request::METHOD_GET, self::URL_TACHES."?$filterName=$filterValue", [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $arrayResponse = json_decode($response->getContent(), true);
        foreach ($arrayResponse as $item) {
            switch ($filterName) {
                case StaticDataEnum::TACHES_TYPES['Tout']:
                    $this->assertNull($item['groupe']);
                    break;
                case StaticDataEnum::TACHES_TYPES['Groupe']:
                    $this->assertNotNull($item['groupe']);
                    break;
                case StaticDataEnum::TACHES_TYPES['Utilisateur']:
                    $this->assertEquals($item['createdBy'], self::URL_USERS.'/'.$this->currentUser->getId());
                    $this->assertNull($item['groupe']);
                    break;
            }
        }
    }

    // Provider get All
    public static function getAllTasksDataProvider(): array
    {
        $created_200 = [
            'filter_tout' => [
                'filterName' => 'type',
                'filterValue' => StaticDataEnum::TACHES_TYPES['Tout'],
            ],
            'filter_groupe' => [
                'filterName' => 'type',
                'filterValue' => StaticDataEnum::TACHES_TYPES['Groupe'],
            ],
            'filter_utilisateur' => [
                'filterName' => 'type',
                'filterValue' => StaticDataEnum::TACHES_TYPES['Utilisateur'],
            ],
        ];

        return $created_200;
    }

    // Provider for User Type Task
    public static function writeUserTypeTaskDataProvider(): array
    {
        $created_201 = [
            'root_can_create_his_own' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Utilisateur',
                    'faite' => false,
                    'createdBy' => '/api/users/1',
                    'priority' => '/api/tache_priorities/1',
                    'groupe' => null,
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_CREATED,
                ],
            ],
        ];

        $forbidden_403 = [
            'root_cant_create_for_another_user' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Utilisateur',
                    'faite' => false,
                    'createdBy' => '/api/users/2',
                    'priority' => '/api/tache_priorities/1',
                    'groupe' => null,
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_FORBIDDEN,
                ],
            ],
        ];

        return array_merge($created_201, $forbidden_403);
    }

    // Provider for Group Type Task
    public static function writeGroupTypeTaskDataProvider(): array
    {
        $created_201 = [
            'root_can_create_for_his_group' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Groupe',
                    'faite' => false,
                    'createdBy' => '/api/users/1',
                    'priority' => '/api/tache_priorities/1',
                    'groupe' => '/api/groupes/1',
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_CREATED,
                ],
            ],
            'root_can_create_for_another_group' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Groupe',
                    'faite' => false,
                    'createdBy' => '/api/users/1',
                    'priority' => '/api/tache_priorities/1',
                    'groupe' => '/api/groupes/2',
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_CREATED,
                ],
            ],
        ];

        return $created_201;
    }

    // Provider for All Type Task
    public static function writeAllTypeTaskDataProvider(): array
    {
        $created_201 = [
            'root_can_create_for_all_users' => [
                'data' => [
                    'libelle' => 'libelle',
                    'contenu' => 'contenu',
                    'type' => 'Tout',
                    'faite' => false,
                    'createdBy' => '/api/users/1',
                    'priority' => '/api/tache_priorities/2',
                    'groupe' => null,
                ],
                'assert' => [
                    'codeResponse' => Response::HTTP_CREATED,
                ],
            ],
        ];

        return $created_201;
    }
}
