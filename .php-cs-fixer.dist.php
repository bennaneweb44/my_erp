<?php

declare(strict_types=1);

use PhpCsFixer\Finder;
use PhpCsFixer\Config;
use PhpCsFixer\Fixer\DoctrineAnnotation\DoctrineAnnotationBracesFixer;

$finder = (new Finder())
    ->in(__DIR__)
    ->exclude('var')
;

$defaultIgnoredTags = (new DoctrineAnnotationBracesFixer())
    ->getConfigurationDefinition()
    ->getOptions()[0]
    ->getDefault()
;

return (new Config())
    ->setRules([
        '@Symfony' => true,
        '@DoctrineAnnotation' => true,
        'doctrine_annotation_braces' => [
            'syntax' => 'with_braces',
            'ignored_tags' => array_merge($defaultIgnoredTags, ['extends']),
        ]
    ])
    ->setFinder($finder)
;
